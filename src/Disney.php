<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        //The end array
        $result = array();
       
        //Grabbing all actors names
        $query = '//Disney/Actors/Actor/Name';
        $actors = $this->xpath->query($query);

        //Putting their names in as a key and making the value an empty array
        foreach($actors as $actor) {
          $result[$actor->nodeValue] = array();
        }

        // Grabbing all Roles
        $query = '//Disney/Subsidiaries/Subsidiary/Movie/Cast/Role';
        $roles = $this->xpath->query($query);
        
        // For each role
        foreach ($roles as $role) {
            // Get the "actor" attribute
            $id = $role->attributes->getNamedItem("actor")->nodeValue;

            // Make a query for an actors name based on the id (same as "actor")
            $query = '//Disney/Actors/Actor[@id = "' . $id . '"]/Name';
            $name = $this->xpath->query($query);

            // Grab the role's parent' parent (movie) so we can grab nodeValue
            // The resulting string is weird in that it has several blank lines and and all values are preceeded with a \t
            // So I split the string based on \n. This results in a array with a bunch of empty indexes
            // However index [1] [2] [3] are name, year, and runtime respectively.
            $movieInfo = explode("\n", $role->parentNode->parentNode->nodeValue);

            // Push to the end of the array at index $result[$name[0]->nodeValue]. This value is whatever name is associated with the 
            // id we grabbed previously. E.g. $result[$name[0]]-> nodeValue is (Robert Downey Jr.)
            // I had to ltrim() due to the previously mentioned \t at the beginning of all values.
            array_push($result[$name[0]->nodeValue], "As " . $role->attributes->getNamedItem("name")->nodeValue . " in " . ltrim($movieInfo[1]) . " (" . ltrim($movieInfo[2]) . ")"); 
        }

        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        // Arrays I will compare to
        $comp1 = array();
        $comp2 = array();

        $query = '//Disney/Subsidiaries/Subsidiary/Movie/Cast/Role/@actor';
        $actors = $this->xpath->query($query);

        // Put all actors (from subsidiaries) into an array
        foreach ($actors as $actor) {
          array_push($comp1, $actor->nodeValue);
        }

        $query = '//Disney/Actors/Actor/@id';
        $employees = $this->xpath->query($query);

        // Put all actors (from actors) into an array
        foreach ($employees as $employee) {
          array_push($comp2, $employee->nodeValue);
        }

        // Find whats in Actors but not Subsidiaries
        $difference = array_diff($comp2, $comp1);

        foreach ($difference as $diff) {
          // Grab the Actor with that id
          $query = '//Disney/Actors/Actor[@id = "' . $diff . '"]';
          $remove = $this->xpath->query($query);
          //Remove them
          $remove[0]->parentNode->removeChild($remove[0]);
        }
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        // Not the cleanest...

        //Create the Element Role.
        $domElement = $this->doc->createElement('Role');

        //Create attributes, give them values (from the parameters) and append them to the element.
        $domAttribute = $this->doc->createAttribute('name');
        $domAttribute->value = $roleName;
        $domElement->appendChild($domAttribute);
        
        //Just checking if there is an alias
        if ($roleAlias) {
          $domAttribute = $this->doc->createAttribute('alias');
          $domAttribute->value = $roleAlias;
          $domElement->appendChild($domAttribute);
        }

        //Same for the third time, could be implemented without running more or less the same code snippet several times
        $domAttribute = $this->doc->createAttribute('actor');
        $domAttribute->value = $roleActor;
        $domElement->appendChild($domAttribute);

        //Find the correct Cast based on parameters given
        $query = '//Disney/Subsidiaries/Subsidiary[@id = "' . $subsidiaryId . '"]/Movie[Name = "'  . $movieName . '" and Year = "' . $movieYear . '"]/Cast';
        $cast = $this->xpath->query($query);

        // Append the element to that Cast
        $cast[0]->appendChild($domElement);
    }
}
?>
